<?php 
function getConstants(){
    $json = file_get_contents(dirname(__FILE__)."/constants.json");
    $constants = json_decode($json, true);
    return $constants;
}
?>