<?php 
require(dirname(__FILE__).'/../../common/getConstants.php');

function getEnlarging($processId,$token,$account_id){
    $variables=getConstants();
    $apiUrl=$variables['api']['url'];

    $cURLConnection = curl_init($apiUrl.'/enlarge/'.$processId.'?accountId='.$account_id);
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER,1); 
    curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array('Content-Type:application/json','x-api-key: '.$token));

    $apiResponse = curl_exec($cURLConnection);
    curl_close($cURLConnection);
    return $apiResponse;
}
?>