<?php 
require_once(dirname(__FILE__).'/../../common/getConstants.php');

function startEnlarging($data,$token){
    $variables=getConstants();
    $apiUrl=$variables['api']['url'];

    $headers = [
        'Content-Type:application/json',
        'x-api-key: '.$token,
    ];

    $cURLConnection = curl_init($apiUrl.'/enlarge/start');
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($cURLConnection, CURLOPT_POST,1);
    curl_setopt($cURLConnection, CURLOPT_POSTFIELDS,$data);  
    curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, $headers);

    $apiResponse = curl_exec($cURLConnection);
    curl_close($cURLConnection);
    return $apiResponse;
}
?>