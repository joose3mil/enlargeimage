<?php
require_once(dirname(__FILE__).'/../common/getConstants.php');

function generateKey( $accountId){
	$variables=getConstants();
    $apiUrl=$variables['api']['url'];

	$data ='{ "accountId" :'.$accountId.'}';
	$cURLConnection = curl_init($apiUrl.'/user/generateKey');
	curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($cURLConnection, CURLOPT_POST,1);
	curl_setopt($cURLConnection, CURLOPT_POSTFIELDS,$data);  
	curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	$apiResponse = curl_exec($cURLConnection);
	return $apiResponse;
}
?>