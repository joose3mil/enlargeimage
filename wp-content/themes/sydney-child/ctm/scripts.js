var cur_enl_pro;
var arg_arr = {};

function ap_rqu_ctm(url, ident) {
  $.post({
    type: "POST",
    url: `${window.location.origin}/wp-content/themes/sydney-child/ctm/callapi.php`,
    data: {
      action: "ap_rqu",
      url: url,
      upscaling: $("input.upscaling[type='radio']:checked").val(),
      no_red: $("input.no_red[type='radio']:checked").val(),
      img_type: $("input.img_type[type='radio']:checked").val(),
      ident,
      userId: $(".accounts_select_widget .accounts_select_widget_select").val(),
    },

    beforeSend: function () {
      $(".loagif .cnt").prepend("<p>Sending parameters ...</p>");
      startProgressBar({ limit: 20, percent: 10 });
      $(".erro_mesg").remove();
    },
    success(response) {
      var got_ident, info, status;
      try {
        info = JSON.parse(response)["info"];
        got_ident = JSON.parse(response)["ident"];
        status = JSON.parse(response)["status"];
      } catch (e) {
        $(".curr_ima_cont#" + ident + " .processing_msg").html(
          "<p style='color: #e41f1f;font-weight: bold;' class='erro_mesg'>Something went wrong. Please, try again and/or contact us to better assist you.</p>"
        );
        $(".loagif").remove();
        $(".curr_ima_cont#" + ident + " .ctm-custom-button").removeAttr(
          "disabled"
        );
      }
      if (status == 200) {
        $(".loagif .cnt p").remove();

        tryGetEnlargingProcess(info, got_ident);
      } else {
        $(".curr_ima_cont#" + ident + " .processing_msg").html(
          "<p style='color: #e41f1f;font-weight: bold;' class='erro_mesg'>" +
            status +
            "</p>"
        );
        $(".loagif").remove();
        $(".curr_ima_cont#" + ident + " .ctm-custom-button").removeAttr(
          "disabled"
        );
      }
    },
    error: function () {
      $(".curr_ima_cont#" + ident + " .processing_msg").html(
        "<div>Something went wrong. Please, try again and/or contact us to better assist you.2</div>"
      );
      $(".curr_ima_cont#" + ident + " .ctm-custom-button").removeAttr(
        "disabled"
      );
      $(".loagif").remove();
    },
  });
}
function show_popup(identy) {
  $(".ctm-form-overlay").show();
  $("#target_to").val(identy);
}

function tryGetEnlargingProcess(info, ident) {
  $(".loagif .cnt").prepend("<p>Enlarging Image ...</p>");
  startProgressBar({ limit: 30, percent: 40 });
  var timesRun = 0;
  let isProcessing = false;
  var interval = setInterval(function () {
    if (!isProcessing) {
      timesRun += 1;
      if (timesRun === 10) {
        clearInterval(interval);
      }
      isProcessing = true;

      get_down_link(info, ident)
        .then((res) => {
          if (res?.status === "failed") {
            $(
              ".curr_ima_cont#" +
                res.HTMLProcessContainerId +
                " .processing_msg"
            ).html(
              "<p style='color: #e41f1f;font-weight: bold;' class='erro_mesg'>Something went wrong. Please, try again and/or contact us to better assist you.</p>"
            );

            $(
              ".curr_ima_cont#" +
                res.HTMLProcessContainerId +
                " .ctm-custom-button"
            ).removeAttr("disabled");

            $(".loagif").remove();
          }
          clearInterval(interval);
          isProcessing = false;
        })
        .catch((res) => {
          $(
            ".curr_ima_cont#" + res.HTMLProcessContainerId + " .processing_msg"
          ).html(
            "<p style='color: #e41f1f;font-weight: bold;' class='erro_mesg'>Something went wrong. Please, try again and/or contact us to better assist you.</p>"
          );

          $(
            ".curr_ima_cont#" +
              res.HTMLProcessContainerId +
              " .ctm-custom-button"
          ).removeAttr("disabled");

          $(".loagif").remove();

          clearInterval(interval);
          console.log(res);
          isProcessing = false;
        });
    }
  }, 30000);
}

const get_down_link = (info, ident) => {
  return new Promise((resolve, reject) => {
    var name_enl = $(".curr_ima_cont#" + ident + " span").text();
    $.post({
      type: "POST",
      url: `${window.location.origin}/wp-content/themes/sydney-child/ctm/callapi.php`,
      data: {
        action: "get_down",
        info: info,
        ident: ident,
        nameenl: name_enl,
        userId: $(
          ".accounts_select_widget .accounts_select_widget_select"
        ).val(),
      },
      success(response) {
        try {
          var status = JSON.parse(response)["status"];
          if (status == "success") {
            console.log(
              '<a class="ctm-download-link" download="' +
                name_enl +
                '" href="' +
                JSON.parse(response)["url"] +
                '">Download</a><br>'
            );
            $(".loagif").remove();

            $(".curr_ima_cont#" + ident + " div").html(
              '<a class="ctm-download-link" download="' +
                name_enl +
                '" href="' +
                JSON.parse(response)["url"] +
                '">Download</a><br>'
            );
            $(".curr_ima_cont#" + ident + " .processing_msg").remove();
          } else if (status == 201) {
            $(".curr_ima_cont#" + ident + " .processing_msg").html(
              "<p style='color: #e6e668;font-weight: bold;' class='erro_mesg'>This is taking longer than usual. If we're able to enlarge your image, it will be sent to your email shortly.</p>"
            );
            $(".curr_ima_cont#" + ident + " .ctm-custom-button").removeAttr(
              "disabled"
            );
            $(".loagif").remove();
          } else if (status == 205) {
            $(".curr_ima_cont#" + ident + " .processing_msg").html(
              "<p style='color: #e41f1f;font-weight: bold;' class='erro_mesg'>This is taking longer than usual. Please, <a href='/my-account'>create yourself an account </a> so we'll enlarge your image and send it to your email.</p>"
            );
            $(".curr_ima_cont#" + ident + " .ctm-custom-button").removeAttr(
              "disabled"
            );
            $(".loagif").remove();
          }
          // else {
          //   $(`.curr_ima_cont#${t} .processing_msg`).html(
          //     `<p style='color: #e41f1f;font-weight: bold;' class='erro_mesg'>${
          //       status
          //         ? status
          //         : "Something went wrong. Please, try again and/or contact us to better assist you."
          //     }</p>`
          //   );
          //   $(`.curr_ima_cont#${t} .ctm-custom-button`).removeAttr("disabled");
          //   $(".loagif").remove();
          // }
          return resolve({
            status: status,
            HTMLProcessContainerId: ident,
          });
        } catch (err) {
          console.log({ err });
          return reject({
            status: "error",
            HTMLProcessContainerId: ident,
          });
        }
      },
      error: function (xmlhttprequest, textstatus, message) {
        return reject({ status: "error", HTMLProcessContainerId: t });
      },
    });
  });
};
function delete_elmnt(target) {
  $("#" + target).remove();
}

function startProgressBar({ limit, percent }) {
  let interval = setInterval(() => {
    if (limit > percent) {
      clearInterval(interval);
    }

    $("#myBar").width(percent + 1 + "%");
  }, 2000);
}
function enalg_msg() {
  $(".loagif .cnt p").text(
    "We're resizing your image. The process will be over shortly."
  );
}
function readURL(input, val) {
  var fileTypes = ["jpg", "jpeg", "png"];
  jQuery(function ($) {
    if (input.files && input.files[0]) {
      var extension = input.files[0].name.split(".").pop().toLowerCase(), //file extension from input file
        isSuccess = fileTypes.indexOf(extension) > -1;
      if (isSuccess) {
        var reader = new FileReader();
        reader.onload = function (e) {
          var image = new Image();
          image.src = e.target.result;
          image.onload = function () {
            cur_enl_pro = Math.floor(100000000 + Math.random() * 900000000);
            if (
              input.files[0].size > 10000000 ||
              this.width > 3000 ||
              this.height > 3000
            ) {
              $("#im_buplr label").after(
                "<div class='curr_ima_cont' id='" +
                  cur_enl_pro +
                  "'><img src='" +
                  e.target.result +
                  "' style='width: 50px;'><span style='color: red;margin-left: 15px;font-weight: bold;'>Over sized</span><div><input type='button' class='ctm-custom-button' onclick='delete_elmnt(\"" +
                  cur_enl_pro +
                  "\")' value='Delete' </div></div>"
              );
            } else {
              var name = input.value;
              name = name.replace(/.*[\/\\]/, "");
              $("#im_buplr label").after(
                "<div class='curr_ima_cont' id='" +
                  cur_enl_pro +
                  "'><img src='" +
                  e.target.result +
                  "' style='width: 50px;'><span>" +
                  name +
                  "</span><div class='btnContainer'><input type='button' class='ctm-custom-button' value='Start' onclick='show_popup(\"" +
                  cur_enl_pro +
                  "\")' </div></div>"
              );
              var files = $("#file")[0].files[0];
              arg_arr[cur_enl_pro] = files;
            }
          };
        };
        reader.readAsDataURL(input.files[0]);
      } else {
        $("#im_buplr ").prepend(
          "<p style='color: #e41f1f;font-weight: bold;' class='erro_mesg pre_error'>Error - file not supported. Please, upload an image file like jpg, jpeg or png.</p>"
        );
      }
    }
  });
}
jQuery(document).ready(function ($) {
  $(".ctm-custom-button").click(function (e) {
    e.preventDefault();
    $(".ctm-form-overlay").show();
  });

  $("#file").change(function () {
    readURL(this, $(this).val());
  });
  $(".smt-trg").click(function (e) {
    $("#file").val("");
    e.preventDefault();
    if (
      $(".accounts_select_widget").length &&
      !$(".accounts_select_widget .accounts_select_widget_select").val()
    ) {
      $(".accounts_select_widget_select").after(
        "<p style='color: #e41f1f;font-weight: bold;' class='erro_mesg'>Please, select an account</p>"
      );
      $(".ctm-form-overlay").hide();
      return false;
    }
    var fd = new FormData();
    var the_targ = $("#target_to").val();
    var salecte_td = arg_arr[$("#target_to").val()];
    fd.append("file", salecte_td);
    fd.append("ident", the_targ);
    $(".ctm-form-overlay").hide();
    $(".pre_error").remove();
    $(".curr_ima_cont#" + the_targ + " .processing_msg").remove();
    $(".curr_ima_cont#" + the_targ + " .ctm-custom-button").attr(
      "disabled",
      "disabled"
    );
    $(".curr_ima_cont#" + the_targ).prepend(
      `<div class="processing_msg"><img src="${window.location.origin}/wp-content/uploads/2020/03/loading-spinner.svg"><p> Processing... </p></div>`
    );
    $.post({
      type: "post",
      url: `${window.location.origin}/wp-content/themes/sydney-child/ctm/callapi.php`,
      data: fd,
      contentType: false,
      processData: false,
      beforeSend: function () {
        $("body").append(
          `<div class='loagif'><div class='cnt'><a href='#' title='Minimize' class='close-icon ctm-close-loading'><i class='fas fa-minus-square'></i></i></a><p>Uploading Image ...</p><div id='myProgress'><div id='myBar'></div></div><img src='${window.location.origin}/wp-content/uploads/2020/03/loading-spinner.svg'></div></div>`
        );
        startProgressBar({ limit: 10, percent: 0 });
        $(".ctm-close-loading").click(function () {
          $(".loagif").hide();
        });
      },
      success(response) {
        $(".loagif .cnt p").remove();
        var status = JSON.parse(response)["status"];
        if (status == 200) {
          width = 30;
          ap_rqu_ctm(
            JSON.parse(response)["url"],
            JSON.parse(response)["ident"]
          );
        } else {
          $(".curr_ima_cont#" + the_targ + " .processing_msg").html(
            "<p style='color: #e41f1f;font-weight: bold;' class='erro_mesg'>" +
              status +
              "</p>"
          );
          $(".curr_ima_cont#" + the_targ + " .ctm-custom-button").removeAttr(
            "disabled"
          );
          $(".loagif").remove();
        }
      },
      error: function () {
        $(".curr_ima_cont#" + the_targ + " .processing_msg").html(
          "<div>Something went wrong. Please, try again and/or contact us to better assist you.</div>"
        );
        $(".curr_ima_cont#" + the_targ + " .ctm-custom-button").removeAttr(
          "disabled"
        );
        $(".loagif").remove();
      },
    });
  });
  $("input.upscaling[type='radio']").on("change", function () {
    $("input.upscaling[type='radio']").not(this).prop("checked", false);
  });
  $("input.no_red[type='radio']").on("change", function () {
    $("input.no_red[type='radio']").not(this).prop("checked", false);
  });
  $("input.img_type[type='radio']").on("change", function () {
    $("input.img_type[type='radio']").not(this).prop("checked", false);
  });
  $(".ctm-close").click(function () {
    $(".ctm-form-overlay").hide();
  });

  $(".download_Image_Checkbox").change(function () {
    var classNameArray = $(this).attr("class").split(" ");
    var className = "." + classNameArray[1];
    $("form.radioRole input[type=radio]").prop("checked", false);
    $("form.radioRole table").hide();
    $("form.radioRole input[type=radio]" + className).prop("checked", true);
    if ($(this).prop("checked") == true) {
      $("table" + className).show();
      console.log(className);
    }
  });
  var clickedInfo = false;
  $("th").mouseover(function () {
    if (clickedInfo == true) {
      $(".dialog").show();
      $("#OrderTablelist").hide();
      clickedInfo = false;
    }

    $(".dialog").css("position", "relative");
  });
  $(".dialog").mouseleave(function () {
    $(".dialog").hide();
    $("#OrderTablelist").show();
  });
  $("tr.orderlist th ").mouseleave(function () {
    if (clickedInfo == false) {
      $(".dialog").hide();
      $("#OrderTablelist").show();
    }
  });
  $("th i.StatusDescription").click(function () {
    $(".dialog").show();
    $("#OrderTablelist").hide();
    clickedInfo = true;
  });
  $(".button-api-configure").click(function () {
    $("#accountList")
      .find("div div h3")
      .each(function () {
        userId = this.id;
      });
    var myId = this.id;
    var accountiD = myId;

    $.ajax({
      type: "POST",
      url: `${window.location.origin}/wp-content/themes/sydney-child/ctm/Api/app/generateToken.php`,
      dataType: "json",
      data: { registration: "success", accounts: accountiD, user: userId },
      success: function (result) {
        console.log(result.publicToken.length);

        // var mydata = JSON.parse(re´sult);
        $("#" + myId).append(
          "<div class='row'><p style='margin-bottom: 1px; margin-top: 5px;' >public key </p> <span class='col-md-12' style='word-wrap: break-word; border-style:solid;font-size: initial;'> " +
            result.publicToken +
            " </span> </div>"
        );
        $("." + myId).hide();
      },
    });
  });
  $(".woocommerce-MyAccount-navigation-link--api_api").append(
    `<ul><li style='margin: auto; border-bottom: unset; margin-left: 24px;'  ><a href='${window.location.origin}/my-account/api_api'/>Api Key</a></li> <li style='margin: auto; border-bottom: unset; margin-left: 24px;'  ><a href='${window.location.origin}/my-account/documentation/'/>documentation</a></li></ul>`
  );
  $(".woocommerce-MyAccount-navigation-link--api_api ul").hide();
  $(".woocommerce-MyAccount-navigation-link--api_api").mouseover(function () {
    $(".woocommerce-MyAccount-navigation-link--api_api ul").show();
  });
  $(".woocommerce-MyAccount-navigation-link--api_api").mouseleave(function () {
    $(".woocommerce-MyAccount-navigation-link--api_api ul").hide();
  });

  $(".documentationLan").click(function () {
    var indetificator = this.id;
    if ($(this).hasClass("node")) {
      $("div." + indetificator + ".node").hide();
      $("div." + indetificator + ".python").show();
    } else if ($(this).hasClass("python")) {
      $("div." + indetificator + ".python").hide();
      $("div." + indetificator + ".node").show();
    }
  });
}); //READY ENDS
