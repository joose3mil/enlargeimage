<?php

require_once(dirname(__FILE__).'/../../../../wp-load.php');
require_once(dirname(__FILE__).'/Api/app/enlarge/getEnlarging.php');
require_once(dirname(__FILE__).'/Api/app/enlarge/startEnlarging.php');
require_once(dirname(__FILE__).'/Api/common/getConstants.php');

if (isset($_POST["action"])&&$_POST["action"]=="ap_rqu"&&isset($_POST["url"])) {
  if (is_user_logged_in()) {
  $account_id=$_POST["userId"];
  if($account_id && !Check_userId($account_id) ) {
    echo '{"status":"You have not access."}';
    return false;
  }
  if(!$account_id){
    $user=get_user_account();
    $account_id= $user->id;
  }

      $ctm_usr_id=get_theuser_idCTM();
      
      $user_role=Get_crr_usr_role($account_id);
          if( current_user_can('administrator') ) {
            $response='{"status":"Admin\'s accounts cannot enlarge images. Please, create a new account."}';
            echo $response;
          }else{
            if(!Check_validated_email()){
                 $response='{"status":"You haven\'t verified your Email. <a href=\''.get_bloginfo('wpurl').'/my-account/\'>Do it here</a> and start enlarging now!"}';
                 echo $response;
                 return false;
            }else if ( $_POST["upscaling"]==4 && $user_role==0) {
                $response='{"status":"Upscaling is not available"}';
                 echo $response;
                 return false;
            }else if (max_enl_img()<1) { 
                  if ($user_role==0) {
                      $upgrade_msg="<a href='#pricing' style='color: #e2dddd;text-decoration: underline;'>Upgrade now</a> to enlarge more images!";
                  }else{
                      $upgrade_msg="Wait for the automatic renewal date. See your account details <a href='/my-account'>here.</a>";
                  }    
                  $response='{"status":"You have exceeded your monthly limit. '.$upgrade_msg.'"}';
                  echo $response;
                  return false;
            }else if(check_limit_date()){
                    $response='{"status":"You have exceeded your date. Please renew your plan "}';
                    echo $response;
                    return false;
            }else{

                ap_rqu($account_id);
          }
        }
    }else{
      $not_loggedUser=Check_not_log_ius_exis();
          if($not_loggedUser){
                if (max_enl_img()<1) {
                    $response='{"status":"You have exceeded your monthly limit. <a href=\'#pricing\' style=\'color: #e2dddd;text-decoration: underline;\'>Upgrade now</a> to enlarge more images!"}';
                    echo $response;
                    return false;
                }else if ( $_POST["upscaling"]==4) {
                     $response='{"status":"Upscaling is not available"}';
                     echo $response;
                     return false;
                }else{

                  ap_rqu($not_loggedUser->id);
              }
          }else{
                if(Create_new_user_ctm(0)){
                  $not_loggedUser=Check_not_log_ius_exis();
                  ap_rqu($not_loggedUser->id);
                }
          }

    }
}

function ap_rqu($account_id){
            $filename=$_POST["url"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $valid_extensions = array("jpg","jpeg","png","gif");

              /* Check file extension */
            if( !in_array(strtolower($ext),$valid_extensions) ) {
                 $response='{"status":"Looks like you tried to upload a non-supported image file. Please, try uploading a jpg, gif, png, jpeg so we can enlarge your image!"}';
                  echo $response;
            }else{
                //$filename="https://www.sas.com/es_cl/navigation/footer/global-footer/_jcr_content/par/styledcontainer/par/image.img.png/1615396200057.png";
                $data='{
                  "upscaling":"'.$_POST["upscaling"].'",
                  "imageType":"'.$_POST["img_type"].'",
                  "noiseReduction":"'.$_POST["no_red"].'",
                  "image":"'.$filename.'",
                  "accountId":'.$account_id.'
                }';
                $CTMvariables=getConstants();
                $adminToken=$CTMvariables['api']['adminToken'];

                $response=startEnlarging($data,$adminToken);
                $res = json_decode($response);

                $processId=$res->processId;
                if (isset($processId)) {
                   $responseToReturn='{"status":"200", "info":"'.$processId.'", "ident": "'.$_POST["ident"].'"}';
                   echo $responseToReturn;
                }else{
                  echo $response;
                }
          }
}

function generateRandomString($length,$path) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
         if (file_exists("upload/".$randomString.".".$path)||$randomString==""||$randomString==null) {
      generateRandomString($length,$path);
     }else{
      return $randomString;
     }
    
}

function getDownload($processId,$name_enl,$user_role,$account_id){
  $CTMvariables=getConstants();
  $adminToken=$CTMvariables['api']['adminToken'];

  $response=getEnlarging($processId,$adminToken,$account_id);
  $res=json_decode($response);
  if($res->status=="success"){

          if ($user_role==0){
            $role_pre_name="FAR";
          }else if ($user_role==1) {
            $role_pre_name="STD";
          }else if ($user_role==2) {
            $role_pre_name="PRO";
          }else{
            $role_pre_name="FNR";
          }

          
          $fileVersion="";
          while(file_exists( __DIR__ . "/enlarged/" .$role_pre_name . "_".$fileVersion. $name_enl)){
            $fileVersion++;
          }
          $filename=$role_pre_name . "_".$fileVersion. $name_enl;

          copy($res->url, __DIR__ . "/enlarged/" .$filename);
          
          $url=get_bloginfo('wpurl').'/wp-content/themes/sydney-child/ctm/enlarged/'. $filename;


          if (is_user_logged_in()) {  
            $userId=get_theuser_idCTM();
            save_enlarge_img($userId,$url,$filename,$account_id);
          }
          
          echo '{"status":"'.$res->status.'", "url":"'.$url.'"}';
          return false;
    }
  echo $response;
  // if ($status =='process' || $status =='new') { 
  //    if ($max_request_numb>1) { 
  //      $max_request_numb--;
  //      getDownload($processId,$ident,$assigned_tkn,$name_enl,$max_request_numb,$user_role,$account_id);
  //     }else{
  //           addBackgroundEnlarging($account_id,$name_enl,$processId);
  //           if (is_user_logged_in()) { 
  //             $responseStatus="201";
  //           }else{
  //           post_download_dicrease($account_id);
  //             $responseStatus="205";
  //           }
  //             echo '{"status":'.$responseStatus.', "ident":"'.$ident.'"}';
  //             return false;
  //           }
  // }else if($status=="success"){
  //       $task_id=$rtr->{$taskid}[1];
  //       $url = $task_id;
  //       $dir = __DIR__ . "/enlarged"; 
  //       $imageFileType = pathinfo($url,PATHINFO_EXTENSION);
  //       $name = $ident;

  //       $filename=$name;
  //       $i=1;
  //       while(file_exists($dir . DIRECTORY_SEPARATOR . $filename .".". $imageFileType)){
  //        $filename=$name . "(" . $i . ")";
  //        $i++;
  //       }
  //       if ($user_role==0){
  //         $role_pre_name="FAR";
  //       }else if ($user_role==1) {
  //         $role_pre_name="STD";
  //       }else if ($user_role==2) {
  //         $role_pre_name="PRO";
  //       }else{
  //         $role_pre_name="FNR";
  //       }
  //       is_dir($dir) || @mkdir($dir) || die("Can't Create folder");
  //       copy($url, $dir . DIRECTORY_SEPARATOR .$role_pre_name . "_" . $filename .".". $imageFileType);
  //       $down_path='/wp-content/themes/sydney-child/ctm/enlarged/' .$role_pre_name . "_" . $filename .".". $imageFileType;


  //       post_download_dicrease($account_id);
  //       $response='{"status":"200", "url":"'.$down_path.'", "ident":"'.$ident.'", "name_enl":"' . $name_enl . '" }';
  //       echo $response;
  //       return $response;

  // }else{
  //   //var_dump($rtr);
  //   $response= '{"status":"error"}';
  //   return $response;
  // }
   
}

if (isset($_POST["action"])&&$_POST["action"]=="get_down"&&isset($_POST["nameenl"])&&isset($_POST["info"])) {
  if (is_user_logged_in()) { 
    if($_POST["userId"]){
      $account_id=$_POST["userId"];
      if(!Check_userId($account_id)) {
        echo '{"status":"You have not access."}';
        return false;
      }
    }else{
      $user=get_user_account();
      $account_id= $user->id;
    }
    $user_role=Get_crr_usr_role($account_id);
  }else{
    $not_loggedUser=Check_not_log_ius_exis();
    $account_id= $not_loggedUser->id;
    $user_role=0;
  }
  $name_enl="Enlarged_".$_POST["nameenl"];

  getDownload($_POST["info"],$name_enl,$user_role,$account_id);
}

if( isset($_FILES['file']['name'] )){
  $filename = $_FILES['file']['name'];

  /* Location */
   $imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
  $location = "upload/".generateRandomString(20,$imageFileType).".".$imageFileType;
  $uploadOk = 1;
 

  /* Valid Extensions */
  $valid_extensions = array("jpg","jpeg","png","gif");
  /* Check file extension */
  if( !in_array(strtolower($imageFileType),$valid_extensions) ) {
     $uploadOk = 0;
  }

  if($uploadOk == 0){
      $response='{"status":"Looks like you tried to upload a non-supported image file. Please, try uploading a jpg, gif, png, jpeg so we can enlarge your image!"}';
      echo $response;
  }else{
     /* Upload file */
     if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
      $ident=$_POST["ident"];
        $response='{"status":"200","ident":"'.$ident.'", "url":"'.get_bloginfo('wpurl').'/wp-content/themes/sydney-child/ctm/'.$location.'"}';
        echo $response;
     }else{

      $response='{"status":"Something were wrong. Please try again"}';
      echo $response;
     }
  }
}


?>