jQuery(document).ready(function ($) {
  $("#change_user_meta_ctm").click(function (e) {
    e.preventDefault();
    var sltc_usr = [];
    $("table.wp-list-table tr td .select_elemt:checked").each(function () {
      var ctm_text = $(this).parent().siblings(".user_id").text();
      sltc_usr.push(ctm_text);
    });
    var plan = $("#datasian").val();
    var ex_date = $("#expire_date").val();
    $.post({
      type: "POST",
      url: `${window.location.origin}/wp-content/themes/sydney-child/ctm/admin_up.php`,
      data: { sltc_usr: sltc_usr, plan: plan, ex_date: ex_date },
      success: function (response) {
        location.reload();
      },
    });
  });

  $(".upd-tkn-ctm").click(function () {
    var tkn_to_upd = $(this).parent().siblings(".tkn_id").text();
    $.post({
      type: "POST",
      url: `${window.location.origin}/wp-content/themes/sydney-child/ctm/admin_up.php`,
      data: { upd_tknctm: true, tkn_id: tkn_to_upd },
      success: function (response) {
        alert(response);
        location.reload();
      },
    });
  });
});
