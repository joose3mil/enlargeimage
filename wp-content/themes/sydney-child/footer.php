<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Sydney
 */
?>
			</div>
		</div>
	</div><!-- #content -->

	<?php do_action('sydney_before_footer'); ?>

	<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
		<?php get_sidebar('footer'); ?>
	<?php endif; ?>

    <a class="go-top"><i class="fa fa-angle-up"></i></a>
		  <div class="ctm-form-overlay">
	  	<div class="content">
	  		<a href='#' class='close-icon ctm-close'><i class='fas fa-times'></i></a>
			  	<div>
			  	<h4>Upscaling</h4>
				  <input type="radio" class="upscaling" name="" value="2X" checked><span>2x</span>
				  <input type="radio" class="upscaling" name="" value="4X"><span>4x</span>
				  <input type="radio" class="upscaling" name="" value="8X"><span>8x</span>
				  <?php 
				  $user_role=Get_crr_usr_role();
				  if ( is_user_logged_in() && $user_role!=0) { ?>
				  <input type="radio" class="upscaling" name="" value="16X"><span>16x</span>
				  <?php } else { ?>
				  		<input type="radio" class="upscaling" name="" value=4 disabled="disabled"><span>16x</span><span style="color: red;margin-left: 12px;">Need 16x upscaling?</span><a href="<?php echo get_bloginfo('wpurl').'/my-account/' ?>" style="color: initial;text-decoration: underline;margin-left: 6px;">Upgrade!</a>
				  <?php } ?> 
			  </div>
			    <div>
			    	<h4>Noise Reduction</h4>
				  <input type="radio" class="no_red" name="" value='none' checked><span>None</span>
				  <input type="radio" class="no_red" name="" value='low'><span>Low</span>
				  <input type="radio" class="no_red" name="" value='medium'><span>Medium</span>
				  <input type="radio" class="no_red" name="" value='high'><span>High</span>
				  <input type="radio" class="no_red" name="" value='highest'><span>Highest</span>
			  </div>
			    <div>
			    	<h4>Image type</h4>
				  <input type="radio" class="img_type" name="" value='artwork' checked><span>Artwork</span>
				  <input type="radio" class="img_type" name="" value='photo'><span>Photo</span>
			  </div>
			<input type="submit" name="" class="smt-trg" value="Upload Image" style="margin-top: 50px;color: #000!important;border: 2px solid #42063A!important">
			<input type="hidden" id="target_to">
	  	</div>
  </div>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<!--/*
<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'sydney' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'sydney' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( __( 'Theme: %2$s by %1$s.', 'sydney' ), 'aThemes', '<a href="https://athemes.com/theme/sydney" rel="nofollow">Sydney</a>' ); ?>
*/-->
			<div style="text-align:center;">
				<span>© <?php echo date('Y') ?> EnlargeImage All rights reserved</span>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

	<?php do_action('sydney_after_footer'); ?>


</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
