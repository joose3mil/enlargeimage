<?php 
//CARGA EL STYLE.CSS DEL TEMA PADRE EN EL TEMA HIJO
 
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
function my_scripts_method() {
	wp_enqueue_script('custom-jquery',get_stylesheet_directory_uri() . '/ctm/jquery-3.4.1.min.js', array( 'jquery' ));
    wp_enqueue_script('custom-script',get_stylesheet_directory_uri() . '/ctm/scripts.js', array( 'jquery' ));
}
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

add_action('personal_options_update', 'my_user_profile_update_action');
add_action('edit_user_profile_update', 'my_user_profile_update_action');
function my_user_profile_update_action($user_id) {
  update_user_meta($user_id, 'artwork_approved', "test");
}

function mytheme_add_woocommerce_support() {
add_theme_support( 'woocommerce' );
}

function gen_ram($n) { 
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
    $randomString = ''; 
  
    for ($i = 0; $i < $n; $i++) { 
        $index = rand(0, strlen($characters) - 1); 
        $randomString .= $characters[$index]; 
    } 
  
    return $randomString; 
} 


add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


add_action( 'user_register', 'myplugin_registration_save', 10, 1 );



function myplugin_registration_save( $user_id ) {
Create_new_user_ctm( $user_id);

}
function Create_new_user_ctm( $user_id){
		$limit_imaeges="";
		if ($user_id==0) {
			$limit_imaeges=2;
		}else{
			$limit_imaeges=10;
		}
		global $wpdb;
		$ip_curr=strval($_SERVER['REMOTE_ADDR']);
        $table = 'wp_users_manage';
        $day = date("Y-m-d H:i:s");
        $limit=date('Y-m-d H:i:s', strtotime('+30days', strtotime($day)));
        $data = array(
            'user_id' => $user_id,
            'ip'    =>$ip_curr,
            'elg_img'    => $limit_imaeges,
            'sign_dat'=> $day,
            'limit_dat'=>$limit,
            'renew_on'=>$limit
        );
        $format = array(
            '%s',
            '%s'
        );
        $success=$wpdb->insert( $table, $data, $format );
        if($success){
        	return true; 
        }else{
        	return false;
        }

}

function addBackgroundEnlarging($user_id,$name,$enlarge_code){
	global $wpdb;
	$table = 'wp_background_process';
	$day = date("Y-m-d H:i:s");
	$limit=date('Y-m-d H:i:s', strtotime('+30days', strtotime($day)));
	$data = array(
		'user_id' => $user_id,
		'img_name'=> $name,
		'img_enlarge_code'=> $enlarge_code,
		'created_at'=>date("Y-m-d H:i:s"),
		'verified_at'=> null
	);
	$format = array(
		'%s',
		'%s'
	);
	$wpdb->insert( $table, $data, $format );

}

function closeBackgroundEnlarging($image_id,$status){
	global $wpdb;
	if($status=="success"){
		$field_to_update='verified_at';
	}else{
		$field_to_update='denied_at';

	}
	$success=$wpdb->update( 'wp_background_process', array($field_to_update=>date("Y-m-d H:i:s")), array('id'=>$image_id) );
}
function get_bg_enl_procces(){
		global $wpdb;
		return $wpdb->get_results( "SELECT id,img_name,user_id,img_enlarge_code FROM `wp_background_process` where `verified_at` IS NULL AND `denied_at` IS NULL");
}
	function renew_to_free_user($id,$extra_data){
		$day = date("Y-m-d H:i:s");
		$limit=date('Y-m-d H:i:s', strtotime('+30days', strtotime($day)));
		$data = array(
			'elg_img'    => 10,
			'sign_dat'=> $day,
			'limit_dat'=>$limit,
			'renew_on'=>$limit,
			'user_role'=>0,
			'email_is_chekd'=> $extra_data
		);
		global $wpdb;
		$wpdb->update( 'wp_users_manage', $data, array('id'=>$id) );
	}

function max_enl_img(){
	global $wpdb;
	if(is_user_logged_in()){
		$user_curr= get_current_user_id();
		$results = $wpdb->get_results( "SELECT elg_img FROM `wp_users_manage` WHERE user_id =$user_curr");
	}else{
		$curr_ip=strval($_SERVER['REMOTE_ADDR']);
		$results = $wpdb->get_results("SELECT elg_img FROM `wp_users_manage`  WHERE ip ='$curr_ip' AND user_id=0 ");
	}
	if(!empty($results)){
	    foreach($results as $row){   
	    $elg_img = $row->elg_img;
	    return $elg_img;
	    }
	}else{
		return false;
	}
}

function Check_not_log_ius_exis(){
	global $wpdb;
	$curr_ip=strval($_SERVER['REMOTE_ADDR']);
	return $wpdb->get_row("SELECT * FROM `wp_users_manage`  WHERE ip ='$curr_ip' AND user_id=0 ");

}
function Check_validated_email(){
	global $wpdb;
	$user_curr= get_current_user_id();
	$results = $wpdb->get_results("SELECT email_is_chekd FROM `wp_users_manage`  WHERE user_id=$user_curr ");
	if(!empty($results)){
	    foreach($results as $row){   
		    $email_is_chekd = $row->email_is_chekd;
		    return $email_is_chekd;
		}
	}else{
		return false;
	}
}

function validate_email(){
	global $wpdb;
	$user_curr= get_current_user_id();
	$wpdb->query($wpdb->prepare("UPDATE `wp_users_manage` SET email_is_chekd=1  WHERE  user_id=$user_curr "));
	if($wpdb){
		return true;
	}else{
		return false;
	}
}


function Get_crr_usr_role($manageUserId=null){
	global $wpdb;
	$user_curr= get_current_user_id();
	if($manageUserId){
		return $wpdb->get_var("SELECT user_role FROM `wp_users_manage` WHERE id=$manageUserId");
	}else{
		$results = $wpdb->get_results("SELECT user_role FROM `wp_users_manage` WHERE user_id =$user_curr");
	}
	if(!empty($results)){
	    foreach($results as $row){   
	    	$elg_img = $row->user_role;
	    	return $elg_img;
		}
	}
}

function get_account_by_accountId($accountId){
	global $wpdb;
	$user_curr= get_current_user_id();
	return $wpdb->get_row( "SELECT * FROM `wp_users_manage`  WHERE `user_id`=$user_curr AND `id`=$accountId");
}

function get_user_account(){
	global $wpdb;
	$user_curr= get_current_user_id();
	return $wpdb->get_row( "SELECT * FROM `wp_users_manage`  WHERE `user_id`=$user_curr");
}

function Check_userId($userId){
	global $wpdb;
	$user_curr= get_current_user_id();
	return $wpdb->get_var( "SELECT user_role FROM `wp_users_manage` WHERE user_id =$user_curr AND id=$userId");
}

function Get_cont_down_img($user_curr){
		global $wpdb;
	$table = 'wp_enlarged_images';
	$results = $wpdb->get_results( "SELECT * FROM `wp_enlarged_images` WHERE user_id =$user_curr");
	return $results;
}

function Get_All_users_details($user_idses){
	global $wpdb;
	$results = $wpdb->get_results( "SELECT * FROM `wp_users_manage` WHERE user_id =$user_idses");
	return $results;
}

function post_download_dicrease($id){
	global $wpdb;
	$table = 'wp_users_manage';
	if(is_user_logged_in()){
		$wpdb->query("UPDATE $table SET elg_img= elg_img -1  WHERE id =$id");
	}else{
		$curr_ip=strval($_SERVER['REMOTE_ADDR']);
		$wpdb->query("UPDATE $table SET elg_img= elg_img -1  WHERE ip ='$curr_ip' AND user_id=0 ");
	}
	if($wpdb->last_error !== ''){
		return true;
	}else{
		return false;
	}
}


function get_assigned_tkn($user_role,$action){
	if($user_role==1){
		return "e582cab15da441078c0d0a9920612a3a";
	}else {
		return "ee05ac16c13d47268199485457406f6e";
	}/*else{
		return "1a7c1fad9064424b8c62ed13dd0d8c04";
	/*
		if($user_role=="none"){
			$unregistered_token=get_unregistered_usr_tkn();
			return $unregistered_token;
		}else if($user_role==0){
			$id=get_current_user_id();
			$user=Get_All_users_details($id);
			foreach($user as $row){   
		    	$token_id = $row->token_id;
		    	if($action=="enlarge"){update_users_inline($token_id,"+1");}
		    	$token=get_free_usr_tkn($token_id);
		    	return $token;
			}
		}
	}*/
}

function check_limit_date(){
	$ctm_user =Get_All_users_details(get_theuser_idCTM());
 	if(!empty($ctm_user)){
	    foreach($ctm_user as $row){   
	    $limit_dat = $row->renew_on;
	    $day = date("YmdHis");
        $limit_dat=date('YmdHis', strtotime(" ", strtotime($limit_dat)));
	        if ($limit_dat<$day) {
	        	return true;
	        }else{
	        	return false;
	        }
	    }
	}
}


function get_theuser_idCTM(){
	global $current_user;
		$getmemid = $current_user->user_login;
		$crr_id= $current_user->ID; 
		return $crr_id;
}
function save_enlarge_img( $user_id, $img_name,$name_enl,$userManageID=null) {

		global $wpdb;
        $table = 'wp_enlarged_images';
        $day = date("Y-m-d H:i:s");
        $data = array(
            'user_id' => $user_id,
            'image_name' => $img_name,
            'down_name'=> $name_enl,
            'date_en'=> $day,
            'user_manages_id'=> $userManageID
        );
        $format = array(
            '%s',
            '%s'
        );
        $success=$wpdb->insert( $table, $data, $format );

}
function wk_custom_endpoint() {
  add_rewrite_endpoint( 'downloaded_img', EP_ROOT | EP_PAGES );
  add_rewrite_endpoint( 'api_api', EP_ROOT | EP_PAGES );
  add_rewrite_endpoint( 'documentation', EP_ROOT | EP_PAGES );
}

add_action( 'init', 'wk_custom_endpoint' );

add_filter( 'woocommerce_account_menu_items', 'wk_new_menu_items' );
function wk_new_menu_items( $items ) {
	$items[ 'downloaded_img' ] = __( 'Downloaded Images', 'webkul' );
	$items[ 'api_config' ] = __( 'Api', 'webkul' );
	return $items;
}

$endpoint = 'downloaded_img';

add_action( 'woocommerce_account_' . $endpoint .  '_endpoint', 'wk_endpoint_content' );
$endpoint_api = 'api_api';
$endpoint_doc = 'documentation';

add_action( 'woocommerce_account_' . $endpoint_api .  '_endpoint', 'wk_endpoint_content_api_config' );
add_action( 'woocommerce_account_' . $endpoint_doc .  '_endpoint', 'wk_endpoint_documentation' );

function wk_endpoint_documentation() {
	require("apiDocumentation.php");
}
function wk_endpoint_content() {
	require("downloaded_img.php");
	download_ImageCont();
}
function wk_endpoint_content_api_config() {
	require("api-config.php"); 
}
function mail_template($head,$body){
$template='<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="width: 40%;margin: 0 auto;">
    			<tbody>
            			<tr>            
							<td id="m_-5878948582922973480header_wrapper" style="font-family: sans-serif;padding: 36px 48px; display: block;    background-color: #42063a;">
								<h1 style="color: #ffffff;">' . $head . '</h1>
							</td>
                        </tr>
                        <tr>
							<td valign="top" style="padding: 48px 48px 32px;">
								<div style="color: #683861;     font-family: sans-serif;color:#42063a;">
										' . $body . '
							 	</div>
							</td>
                                                        
        				</tr>
        				<tr>
				            <td style="font-family: sans-serif;padding: 36px 48px; display: block;text-align: center;">
				                    <a
				                        href="'.get_bloginfo('wpurl').'"
				                        style="color: #42063a; font-weight: normal; text-decoration: underline;">
				                        <img
				                            src="'.get_bloginfo('wpurl').'/wp-content/uploads/2020/10/enlargeimage-logo-250x31-purple.png"
				                            style="
				                                border: none;
				                                display: inline-block;
				                                font-size: 14px;
				                                font-weight: bold;
				                                height: auto;
				                                outline: none;
				                                text-decoration: none;
				                                text-transform: capitalize;
				                                vertical-align: middle;
				                                margin-right: 10px;
				                                max-width: 100%;
				                            "
				                        />
				                    </a>
				            </td>
                        </tr>

				    </tbody>
				</table>
';
return $template;
}
function my_custom_my_account_menu_items( $items ) {
    $items = array(
    	'downloaded_img'      => 'Downloaded Images',
        //'dashboard'         => __( 'Dashboard', 'woocommerce' ),
        'orders'            => __( 'Orders', 'woocommerce' ),
        //'downloads'       => __( 'Downloads', 'woocommerce' ),
        //'edit-address'    => __( 'Addresses', 'woocommerce' ),
        //'payment-methods' => __( 'Payment Methods', 'woocommerce' ),
        'api_api'               => 'Api' ,
        'edit-account'      => __( 'Edit Account', 'woocommerce' ),
        'customer-logout'   => __( 'Logout', 'woocommerce' ),
    );

    return $items;
}
/*Adding admin label Upgrade Users*/
add_filter( 'woocommerce_account_menu_items', 'my_custom_my_account_menu_items' );


function my_admin_menu() {
		$menu=add_menu_page(
			__( 'Upgrade Users', 'my-textdomain' ),
			__( "Upgrade Users' Privileges", 'my-textdomain' ),
			'manage_options',
			'upgrade-users',
			'my_admin_page_contents',
			'dashicons-admin-users',
			3
		);
   add_action( 'admin_print_scripts-' . $menu, 'admin_custom_js' );
}
function get_all_user_ctm(){
	$args = array('orderby' => 'display_name');
	$wp_user_query = new WP_User_Query($args);
	$authors = $wp_user_query->get_results();
	return $authors;
}

add_action( 'admin_menu', 'my_admin_menu' );

function admin_custom_js(){
    wp_enqueue_script( 'my-script',get_stylesheet_directory_uri() . '/ctm/admin_scripts.js', array( 'jquery-ui-core', 'jquery-ui-tabs' ) );
 }
function wc_fr_add_orders_to_account() {
	session_start();
		$check_email_add=Check_validated_email();
	 if($check_email_add==0){
	 	echo "<script>if (window.history.replaceState) {window.history.replaceState(null, null, window.location.href);}</script>";
		 if(!empty($_POST["scr_cde"]) &&!empty($_POST["srt_vrf"])&& $_POST["srt_vrf"]==$_SESSION['scr_tkn']){
				if($_POST["scr_cde"]==$_SESSION['scr_cde']){
					if(validate_email()){
						echo"Your email has been verified";
					}else{
						echo "<p>Something was wrong</p>";
					}
				}else{			 	
		 				echo '<h3>The code is incorrect</h3><form method="POST"><input type="text" name="scr_cde"  value="Verify Now" placeholder="Type the code here" required><input type="submit" name="" value="Verify Now"></form>';
		 		}
		 }else if (!empty($_POST["srt_vrf"])&& $_POST["srt_vrf"]==$_SESSION['scr_tkn']) {
		 		global $current_user;
				get_currentuserinfo();
				$to = $current_user->user_email;
			 	$scr_cde=gen_ram(8);
			 	$_SESSION['scr_cde']=$scr_cde;
			 	$scr_tkn=gen_ram(15);

			 	$headers = array('Content-Type: text/html; charset=UTF-8','From:  EnlargeImage: <Welcome@enlargeimage.com>');

			 	$head="Thanks for using Enlarge Image";
			 	$body='<p style="margin: 0 0 16px;">Hi '.$current_user->user_login.',</p>
				<p style="margin: 0 0 16px;"></p>
				<p style="margin: 0 0 16px;">Please type this code in the text box to verify your email address: '.$scr_cde.'</p>';

			 	$mail_message=mail_template($head,$body);
			 	if(wp_mail( $to, "Please verify your email to start Enlarging", $mail_message , $headers)){
			 		echo '<h3>We sent you a code to your email address</h3><span>Please, remember to check the spam/junk section on your email</span><form method="POST"><input type="hidden" name="srt_vrf" value="'.$scr_tkn.'"><input type="text" name="scr_cde"   placeholder="Type the code here" required><input type="submit" name="" value="Verify Now"></form>';
			 	}else{
			 		echo "<p>The email couldn't be sent</p>";	
			 	}
			 	$_SESSION['scr_tkn']=$scr_tkn;
		 }else{
		 		$scr_tkn=gen_ram(15);
		 		$_SESSION['scr_tkn']  = $scr_tkn;
		 		echo '<p>You haven’t verified your email address, please do it so you can start enlarging images!</p><form method="POST"><input type="hidden" name="srt_vrf" value="'.$scr_tkn.'"><input type="submit" name="" value="Verify Now"></form>';
		 }
	 }
    return $fr_account_orders;
}
add_filter( 'woocommerce_account_dashboard', 'wc_fr_add_orders_to_account' );

function setUserole($role){
	if($role==0){
		return "Free";
	}else if($role==1){
		return "Standard";
	}elseif($role==2){
		return "Pro";
	}
}
function get_images_for_role($role){
	if ($role==0){
		return 10;
	}else if ($role==1) {
		return 200;
	}else if ($role==2) {
		return 500;
	}
}

function my_admin_page_contents() {
	$authors = get_all_user_ctm();
	if (!empty($authors)) {?>
		<table class="wp-list-table widefat fixed striped">
				<thead>
				 	<tr>
				 		<th><input type="checkbox" name=""></th>
				 		<th>User id</th>
				 		<th>Email</th>
				 		<th>Username</th>
				 		<th>User role</th>
				 		<th>Sign up date</th>
				 		<th>Account expires in</th>
				 		<th>Renew on</th>
				 	</tr>
				</thead>
		<?php foreach ($authors as $author) {
			$author_info = get_userdata($author->ID);
			$author_details = Get_All_users_details($author->ID);
			 if(!empty($author_details)){
	    		foreach($author_details as $row):?> 
					<tr> 
						<td><input type="checkbox" class="select_elemt" name=""></td>
						<td class="user_id"><?php echo $row->user_id ?></td>
						<td><?php echo $author_info->user_email ?></td>
						<td class="user_login"><?php echo $author_info->user_login ?></td>
						<td class="user_role"><?php echo setUserole($row->user_role) ?></td>
						<td><?php echo $row->sign_dat ?></td>
						<td><?php echo $row->limit_dat ?></td>
						<td><?php echo $row->renew_on ?></td>
					</tr>
				<?php endforeach; 
			}
		}?>
	</table>
<div class="tablenav bottom">
	<div class="alignleft actions bulkactions">
		<label for="bulk-action-selector-bottom" class="screen-reader-text">Select bulk action</label>
		<select name="action2" id="bulk-action-selector-bottom">
			<option value="-1">Bulk Actions</option>
			<option value="delete">Delete</option>
		</select>
	<input type="submit" id="doaction2" class="button action" value="Apply">
</div>
<form method="post">
<div class="alignleft actions">
	<label class="screen-reader-text" for="datasian">Change user plan…</label>
	<select name="datasian" id="datasian">
		<option value="">Change user plan…</option>			
		<option value="free">Free</option>
		<option value="standard">Standard</option>
		<option value="pro">Pro</option>
	</select>
	<label class="screen-reader-text" for="expire_date">Update Expiration date…</label>
	<select name="expire_date" id="expire_date">
		<option value="">Update Expiration date…</option>			
		<option value="1">1 Month</option>
		<option value="2">2 Months</option>
		<option value="3">6 Months</option>
	</select>
	<input type="submit" name="change_user_meta_ctm" id="change_user_meta_ctm" class="button" value="Change">
</div>
</form>
	<?php } else {
		echo 'No results';
	}
}



add_filter( 'woocommerce_min_password_strength', 'reduce_min_strength_password_requirement' );
function reduce_min_strength_password_requirement( $strength ) {
    // 3 => Strong (default) | 2 => Medium | 1 => Weak | 0 => Very Weak (anything).
    return 1; 
}

add_filter('password_hint', 'change_password_hint');
function change_password_hint($hint) {
  return "Hint: The password should be at least four characters long. To make it stronger, you may use letters and numbers.";
}

function store_mall_wc_empty_cart_redirect_url() {
        $url = get_bloginfo('wpurl').'/#pricing';
        return esc_url( $url );
    }
	
	add_action( 'woocommerce_thankyou', 'so_payment_complete' );
	function so_payment_complete( $order_id ){
		$order = wc_get_order( $order_id );
		$user = $order->get_user();
		if( $user ){
			$headers = array('Content-Type: text/html; charset=UTF-8','From:  EnlargeImage: <Welcome@enlargeimage.com>');
			$admin_email=get_bloginfo('admin_email');
			$head="New purchase";
			$body='<p style="margin: 0 0 16px;">A new purchase has been done.</p>';
			$body.='<p style="margin: 0 0 16px;">Please go here to proccess it:'.get_bloginfo('wpurl').'/wp-admin/post.php?post='.$order_id.'&action=edit</p>';
			$mail_message=mail_template($head,$body);
			wp_mail( 'joose3mil@gmail.com', "A new purchase has been done", $mail_message , $headers);
			// wp_mail( $admin_email, "A new purchase has been done", $mail_message , $headers);
		}
	}
	 add_filter( 'woocommerce_return_to_shop_redirect', 'store_mall_wc_empty_cart_redirect_url' );






	function sv_wc_add_order_meta_box_action( $actions ) {
		
	
		// add "mark printed" custom action
		$actions['ns_fba_send_to_fulfillment'] = __( 'Upgrade Plan', 'Upgrade Plan' );
		return $actions;
	}
	add_action( 'woocommerce_order_actions', 'sv_wc_add_order_meta_box_action' );


	
	function process_order_meta_box_actions( $order ) {
		global $wpdb;
		$items = $order->get_items();
		$OrderStatus= $order->get_status();
		if ($OrderStatus!="completed") {
			foreach ( $items as $item ) {
				$product_name = $item->get_name();
				$user_id = $order->get_user_id();  
				updatUserPlan($user_id,$product_name);
			}

			$accounts = $wpdb->get_results( "SELECT * FROM `wp_users_manage` WHERE user_id =$user_id");
				foreach($accounts as $account){
					if ($account->user_role==0) {
						ChandgeUserManageId($user_id);
						deleteFreeAccount($account->id);
						}
				} 
			$orderid = wc_get_order( $order );
	    	$orderid->update_status( 'completed' );
		}
		
	}
	
	add_action( 'woocommerce_order_action_ns_fba_send_to_fulfillment',  'process_order_meta_box_actions'  );




	function updatUserPlan($userId,$product_name){
		if ($product_name=="Pro") {
			$data['user_role']=2;
			$data['elg_img']=500;
			$limiDateString="+6 months";
		}
		elseif ($product_name=="Standard") {
			$data['user_role']=1;
			$data['elg_img']=200;
			$limiDateString="+2 months";
		}
		elseif ($product_name=="Free") {
			$data['user_role']=0;
			$data['elg_img']=10;
			$limiDateString="+1 months";
		}else{
			$data['user_role']=0;
			$data['elg_img']=10;
			$limiDateString="+1 months";

		}
		$data['sign_dat']=date('Y-m-d H:i:s');
		$data['limit_dat']=date('Y-m-d H:i:s', strtotime($limiDateString));
		$data['renew_on']=date('Y-m-d H:i:s', strtotime("+1 months"));
		$data['user_id']=$userId;
		$data['email_is_chekd']=1;

		global $wpdb;
		$wpdb->insert( 'wp_users_manage', $data);		
	}


 

	// Creating the widget 
class wpb_widget extends WP_Widget {
  
	function __construct() {
	parent::__construct(
	  
	// Base ID of your widget
	'accounts_select_widget', 
	  
	// Widget name will appear in UI
	__('Accounts Select Widget', 'wpb_widget_domain'), 
	  
	// Widget description
	array( 'description' => __( 'Show the accounts of a user in a select', 'wpb_widget_domain' ), ) 
	);
	}
	  
	public function widget( $args, $instance ) {
		if ( is_user_logged_in()):
			$id=get_current_user_id();
			$user_info=Get_All_users_details($id);
			if(count($user_info)>1):?>
				<div class="accounts_select_widget" style="text-align: center;"> 
					<select class="accounts_select_widget_select" style="border-radius: 9px;">
						<option value=''>Select an account</option>
						<?php				
							for ($i=0; $i < count($user_info); $i++):
								$user_role=setUserole($user_info[$i]->user_role);
								?>
								<option value='<?php echo $user_info[$i]->id ?>'>
									<?php echo $user_role.' ('.$user_info[$i]->elg_img.')'?>
								</option>
							<?php
							endfor;
						?>
					</select>
				</div>
				<?php
			endif;
		endif;
	}
}


	 
	// Register and load the widget
	function wpb_load_widget() {
		register_widget( 'wpb_widget' );
	}
	add_action( 'widgets_init', 'wpb_load_widget' );
	function ChandgeUserManageId($Id) {
		global $wpdb;
		// Change UserManageId to 0
		$wpdb->query($wpdb->prepare("UPDATE `wp_enlarged_images` SET user_manages_id=0  WHERE  user_manages_id=$Id"));
		
	}
	function deleteFreeAccount($account){
		global $wpdb;
		$wpdb->query($wpdb->prepare("DELETE FROM `wp_users_manage` WHERE id = $account"));


	}

add_filter( 'wc_add_to_cart_message', 'my_add_to_cart_function', 10, 2 ); 

function my_add_to_cart_function( $message, $product_id ) { 
    $message = sprintf(esc_html__('%s has been added to your cart. This package entails: %s','woocommerce'), get_the_title( $product_id ), get_the_excerpt( $product_id ) ); 
    return $message;
    
}
function removing_customer_details_in_emails( $order, $sent_to_admin, $plain_text, $email ){
    $wmail = WC()->mailer();
    remove_action( 'woocommerce_email_customer_details', array( $wmail, 'email_addresses' ), 20, 3 );
}
add_action( 'woocommerce_email_customer_details', 'removing_customer_details_in_emails', 5, 4 );

// geneate api keys
function button1() {
	
	$accountId=get_current_user_id();
	$tokensDB= getApiKeys($accountId);
	if (empty($tokensDB)) {
		$publicKey = "apikey";
		$secretKey = "secretkey";
		$salt = mt_rand();
		$signatureP = hash_hmac('sha256', $salt, $publicKey, true);
		$signature = hash_hmac('sha256', $salt, $secretKey, true);
		$EncodedSignatureP = base64_encode($signatureP);
		$EncodedSignature = base64_encode($signature);
		$publicEncodedSignature = urlencode($EncodedSignatureP);
		$secretEncodedSignature = urlencode($EncodedSignature);
		//insertDb($publicEncodedSignature, $secretEncodedSignature, $accountId);
		return array ($publicEncodedSignature, $secretEncodedSignature);
	}
	
     } 
function insertDb($publicKey, $privateKey, $accountId){
	global $wpdb;
	$table = 'api_token';
	$data = array(
		'accountId' => $accountId,
		'privateToken'=> $privateKey,
		'publicToken'=> $publicKey
	);
	$wpdb->insert( $table, $data);

     }
	 
	 
	 function GetAccounts($userId){
		 global $wpdb;
		 $query=$wpdb->get_results( "SELECT id FROM `wp_users_manage` WHERE user_id =$userId"); 
		 return $query;
		} 
		
		function getApiKeys($accountId){
		   global $wpdb;
			  return $wpdb->get_var( "SELECT publicToken FROM `api_token` where `accountId` =$accountId ");
		   }

	function getUserNameRole($accountId){
	 	$account= Get_crr_usr_role($accountId);
	 	if ($account=="2") {
	 		$accountName= "Pro";
			
		}
		elseif ($account=="1") {
			$accountName= "Standard";

		}
		elseif ($account=="0") {
			$accountName= "Free";
		}	
		return $accountName;


	 }



?>








