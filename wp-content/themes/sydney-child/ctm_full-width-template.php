<?php

/*

Template Name: ctm Full width

*/
	get_header();
?>



	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="woocommerce">
		<?php 
		if (is_user_logged_in()) {
		 do_action( 'woocommerce_account_navigation' );
		 echo '<div class="woocommerce-MyAccount-content">	'; 
		}
		 ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php

					if ( comments_open() || '0' != get_comments_number() ) :

						comments_template();

					endif;
				?>

			<?php endwhile; // end of the loop. ?>
		<?php 
		if (is_user_logged_in()) {
		 echo '</div>'; 
		}
		 ?>
 			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>