<?php  
?>

<div class="container">
		<h3 class="documentationMenu" >Documentation</h3>
		<hr style="width:50%; height: 5px;border: 0px;background-color: black">
	<div class="row auht-logins ens" >
		<div class="col-md-7 documentationApi">
			<div class="row">
				<div class="col-md-3">
					<h3>User Module</h3>
				</div>
				
				
			</div>
			<h5>Get Downloaded Images:</h5>
			<p>Description: Return all the enlarged image of a given account</p>
			<p>Route: GET http://api.enlargeimage.com/user/downloaded</p>
			<pre>
header:{
    x-api-key: publicKey,
}  			</pre>
			<pre>
response:[
    {
        "imageUrl": String,
        "createAt": Date
    }
]			</pre>
			<div class="row Downloaded node" >
				<div class="col-md-3">
					<p>EXAMPLE:</p>
				</div>
				<div class="col-md-3">
					<input id="Downloaded" class="documentationLan node" type="button" name="changeLanguaje" value="python">
				</div>
				
				<div class="col-md-12"> 
					<pre>
const axios = require("axios");
const headers={
    'x-api-key': YOUR_ACCOUNT_TOKEN,
}

axios.get('http://api.enlargeimage.com/user/downloaded', {headers})
.then(({data}) => data)
.catch(e => e);
					</pre>		
				</div>
			</div>
			<div class="row Downloaded python"  style="display: none">
				<div class="col-md-3">
					<p>EXAMPLE:</p>
				</div>
				<div class="col-md-3">
					<input id="Downloaded" class="documentationLan python" type="button" name="changeLanguaje" value="node">
				</div>
				
				<div class="col-md-12"> 
				<pre>
import requests
r=requests.get("http://api.enlargeimage.com/user/downloaded", headers={"x-api-key":"YOUR_ACCOUNT_TOKEN"})
print(r.text)</pre>		
				</div>
			</div>
			<h5>Get account details:</h5>
			<p>Description: Return all details of an account.</p>
			<p>Route: GET http://api.enlargeimage.com/user/account</p>
			<pre>
Header:{
    x-api-key: publicKey,
}    		</pre>
			<pre>
response:[
    {
        "role": String,
     "remainingImages": INT,
    "renovationDate": Date,
        "planExpirationDate": Date,
    }
]			</pre>
			<div class="row account node" >
				<div class="col-md-3">
					<p>EXAMPLE:</p>
				</div>
				<div class="col-md-3">
					<input id="account" class="documentationLan node" type="button" name="changeLanguaje" value="python">
				</div>
				
				<div class="col-md-12"> 
					<pre>
const axios = require("axios");
const headers={
    'x-api-key': YOUR_ACCOUNT_TOKEN,
}
axios.get('http://api.enlargeimage.com/user/account', {headers})
.then(({data}) => data)
.catch(e => e);		</pre>		
				</div>
			</div>
			<div class="row account python"  style="display: none">
				<div class="col-md-3">
					<p>EXAMPLE:</p>
				</div>
				<div class="col-md-3">
					<input id="account" class="documentationLan python" type="button" name="changeLanguaje" value="node">
				</div>
				
				<div class="col-md-12"> 
				<pre>
import requests
r=requests.get("http://api.enlargeimage.com/user/account", headers={"x-api-key":"YOUR_ACCOUNT_TOKEN"})
print(r.text)</pre>		
				</div>
			</div>
		</div>
	</div>

	<div class="row others en" >
		<div class="col-md-7 documentationApi">
			<div class="row">
				<div class="col-md-3">
					<h3>Enlarge Module</h3>
				</div>
				
				
			</div>
			<h5>Start Enlarging Process:</h5>
			<p>Description: Start Enlarging Process for a given image with the given parameters, and returns an id to track the process.</p>
			<p>Route: POST http://api.enlargeimage.com/enlarge/start</p>
			<h5>Header:</h5>
			<pre>
{
    x-api-key: publicKey,
}  			</pre>
			<h5>Payload:</h5>
			<pre>
//data sent as body
{
    "image": String, 
    "upscaling": String, 
    "noiseReduction": String, 
    "imageType": String, 

	//noiseReduction: aceptedValues:'none'|'low'|'medium'|'high'| 'highest'
	//upscaling aceptedValues:'2X'|'4X'|'8X'|'16X',
	//imageType aceptedValues: 'artwork' or 'photo'

}    		</pre>
			<pre>
response:[
    {
         "processId": String,
    }
]			</pre>
			<div class="row start node" >
				<div class="col-md-3">
					<p>EXAMPLE:</p>
				</div>
				<div class="col-md-3">
					<input id="start" class="documentationLan node" type="button" name="changeLanguaje" value="python">
				</div>
				
				<div class="col-md-12"> 
					<pre>
const axios = require("axios");
const headers={
    'x-api-key': YOUR_ACCOUNT_TOKEN,
}
axios.post('http://api.enlargeimage.com/enlarge/start', { 
  image: YOUR_IMAGE_URL,
  upscaling: "2X",
  noiseReduction: "none",
  imageType: "artwork" },
  {headers})
  .then(({data}) => data)
  .catch(e => e);   </pre>		
				</div>
			</div>
			<div class="row start python"  style="display: none">
				<div class="col-md-3">
					<p>EXAMPLE:</p>
				</div>
				<div class="col-md-3">
					<input id="start" class="documentationLan python" type="button" name="changeLanguaje" value="node">
				</div>
				
				<div class="col-md-12"> 
				<pre>
import requests
header={"x-api-key":"YOUR_IMAGE_URL"}
payload={
  "image": "YOUR_IMAGE_URL",
  "upscaling": "2X",
  "noiseReduction": "none",
  "imageType": "artwork" 
}  
r=requests.post("http://api.enlargeimage.com/enlarge/start",headers=header,data= payload )
print(r.text)</pre>		
				</div>
			</div>

			<h5>Get Enlarging Process:</h5>
			<p>Description: Returns the status of an enlarging process.</p>
			<p>Route: GET http://api.enlargeimage.com/enlarge/processId</p>
			<h5>Header:</h5>
			<pre>
{
    x-api-key: publicKey,
}  			</pre>
			<h5>Payload:</h5>
			<pre>
//data sent as body
{
    'processId': String, 
}   		</pre>
			<div class="row processId node" >
				<div class="col-md-3">
					<p>EXAMPLE:</p>
				</div>
				<div class="col-md-3">
					<input id="processId" class="documentationLan node" type="button" name="changeLanguaje" value="python">
				</div>
				
				<div class="col-md-12"> 
					<pre>
const axios = require("axios");
const headers={
    'x-api-key': YOUR_ACCOUNT_TOKEN,
}
const data={ 
	processId: YOUR_PROCESS_ID,  
}
axios.get('http://api.enlargeimage.com/enlarge/processId',{headers,data})
.then(({data}) => data)
.catch(e => e);		</pre>		
				</div>
			</div>
			<div class="row processId python"  style="display: none">
				<div class="col-md-3">
					<p>EXAMPLE:</p>
				</div>
				<div class="col-md-3">
					<input id="processId" class="documentationLan python" type="button" name="changeLanguaje" value="node">
				</div>
				
				<div class="col-md-12"> 
				<pre>
import requests
header={"x-api-key":"YOUR_ACCOUNT_TOKEN"}
payload={
    "processId": "YOUR_PROCESS_ID" 
}  
r=requests.get("http://api.enlargeimage.com/enlarge/processId",headers=header,data= payload )
print(r.text)</pre>		
				</div>
			</div>

		</div>
	</div>


	
</div>
