<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
//
 

$id=get_current_user_id();
$user_info=Get_All_users_details($id);
for ($i=0; $i < count($user_info); $i++):
	if ($user_info[$i]->email_is_chekd!=0){
		$expire=date('Y-m-d H:i:s', strtotime(" ", strtotime($user_info[0]->limit_dat)));
		$day = date("Y-m-d H:i:s");
		if ($day>$expire) {
			echo "<h3 style='color:red;'>Your Plan has expired</h3>";
		}
		?>
		
			<p>Here is your account information:</p>
				<?php 
				$user_role=setUserole($user_info[$i]->user_role);
				$images_amount=get_images_for_role($user_info[$i]->user_role);
				$remaining_img= intval($images_amount) - intval($user_info[$i]->elg_img);
				if ($user_role=="Standard") {
					$PlanDuration ="2 months";
				}
				elseif ($user_role=="Pro") {
					$PlanDuration ="6 months";
				}
				elseif ($user_role=="Free") {
					$PlanDuration ="1 months";
				}
				 ?>
				<table>
					<thead>
						<td>Current plan</td>
						<td>Package start date</td>
						<td>Next image limit restore date</td>
						<td>Plan expiration date</td>
						<td>Enlarged images</td>
						<td>Plan duration</td>
					</thead>
					<tr>
						<td><?php echo $user_role?></td>
						<td><?php echo date('m-Y-d', strtotime(" ", strtotime($user_info[$i]->sign_dat)))?></td>
						<td><?php echo date('m-Y-d', strtotime(" ", strtotime($user_info[$i]->renew_on)))?></td>
						<td><?php echo date('m-Y-d', strtotime(" ", strtotime($user_info[$i]->limit_dat)))?></td>
						<td><?php echo $remaining_img ." of ".$images_amount?></td>
						<td><?php echo $PlanDuration?></td>
					</tr>
				</table>
			<br><a href="<?php echo get_home_url(); ?>" class="ctm_brn_purp" role="button">Start resizing now</a>
			


	<?php 
	}else{
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */


		do_action( 'woocommerce_account_content' );


	}
endfor
	?>

